
<?php

    require_once "php/DBConnect.php";

?>


<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report</title>
    <meta name="description" content="Your Description Here">
    <meta name="keywords" content="bootstrap themes, portfolio, responsive theme">
    <meta name="author" content="ThemeForces.Com">

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css"  href="css/report.css">

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
	<style>
		ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #1f1f1f;
}

  #tf-footer{
  	position: fixed;
  	width: 100%;
  	bottom: 0px;
  }

/* Float the list items side by side */
ul.tab li {float: left;}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 24px;
	color: lightgrey;
}

/* Change background color of links on hover */
ul.tab li a:hover {background-color: #d35400;}

/* Create an active/current tablink class */
ul.tab li a:focus, .active {background-color: #d35400;}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border-top: none;
}
.tabcontent {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
tr{
  border-bottom:10px;
  }
  #styltext
  {
  margin-top : 50px;
  margin-left : 50px;
  text-indent: 50px;
  font-size : 50px;

  }
  #stylebutton
  {

	font-size:14px;
	width : 100px;
	background-color: #d35400;
	border-radius : 4px;
	border : 0px;
	padding:10px;
  }
    #stylinput
  {
	    height: 40px;
    width: 300px;

  border-radius : 4px;
  color:black;
  background-color: lightgrey;
  }
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="font-family: sans-serif; ">
    <div id="tf-home">
        <div class="overlay">
            <div id="sticky-anchor"></div>
            <nav id="tf-menu" class="navbar navbar-default stick">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand logo" href="index.html"><img src="img/main.png" style="position: fixed; top: 15px; left: 200px; height="50px"; width="50px;"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="homepage.html">Home</a></li>
                        <li><a href="map_page.php">Rescue Team</a></li>
                        <li><a href="report.html">Report</a></li>
                        <li><a href="Contactus.html">Contact</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
	<br><br><br><br>
	<ul class="tab">
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Passed away')" id="defaultOpen">Lives Lost</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Missing')">Missing</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Found')">Found</a></li>
</ul>

<div id="Passed away" class="tabcontent">
  <form >
<input type="text" name="name" id="stylinput"> <input type="submit" value="SEARCH" name="submitted" id="stylebutton">

	</form><br>
    <div class="catalogue">
        <!-- <div class="book-card" id="45">
            <img src="img/male.png" class="book-cover"/>
             <div class="title">
                 <span class="name">Name</span>

             </div>


             <div class="no-of-books">
               <span class="number">Age</span>
             </div>


              <div class="no-of-books">
                <span>email id of corr</span>
              </div>


           <div class="no-of-books">
             <span>Mobie no. of corr</span>
           </div>

        </div> -->

        <?php


            $table = $_GET['table'];
            $query = "SELECT * FROM  `".$table."` WHERE status = \"Dead\"";
            $result = mysqli_query($conn, $query);


            while ($row = mysqli_fetch_assoc($result)) {

                echo "<div class=\"book-card\" id=\"45\">
                    <img src=\"".$row['img_url']."\" class=\"book-cover\"/>
                     <div class=\"title\">
                         <span class=\"name\">".$row['name']."</span>
                     </div>


                     <div class=\"no-of-books\">
                        <span class=\"number\">".$row['age']."</span>
                     </div>


                      <div class=\"no-of-books\">
                        <span>".$row['email']."</span>
                      </div>


                   <div class=\"no-of-books\">
                     <span>".$row['phone']."</span>
                   </div>

                </div>";

            }

         ?>

    </div>
</div>

<div id="Missing" class="tabcontent">
    <form >
<input type="text" name="name" id="stylinput"> <input type="submit" value="SEARCH" name="submitted" id="stylebutton">

	</form><br>
    <div class="catalogue">

        <?php


            $table = $_GET['table'];
            $query = "SELECT * FROM  `".$table."` WHERE status = \"Missing\"";
            $result = mysqli_query($conn, $query);


            while ($row = mysqli_fetch_assoc($result)) {

                echo "<div class=\"book-card\" id=\"45\">
                    <img src=\"".$row['img_url']."\" class=\"book-cover\"/>
                     <div class=\"title\">
                         <span class=\"name\">".$row['name']."</span>
                     </div>


                     <div class=\"no-of-books\">
                        <span class=\"number\">".$row['age']."</span>
                     </div>


                      <div class=\"no-of-books\">
                        <span>".$row['email']."</span>
                      </div>


                   <div class=\"no-of-books\">
                     <span>".$row['phone']."</span>
                   </div>

                </div>";

            }

         ?>

    </div>
</div>

<div id="Found" class="tabcontent">
    <form >
<input type="text" name="name" id="stylinput"> <input type="submit" value="SEARCH" name="submitted" id="stylebutton">

	</form><br>
    <div class="catalogue">

        <?php


            $table = $_GET['table'];
            $query = "SELECT * FROM  `".$table."` WHERE status = \"Rescued\"";
            $result = mysqli_query($conn, $query);


            while ($row = mysqli_fetch_assoc($result)) {

                echo "<div class=\"book-card\" id=\"45\">
                    <img src=\"".$row['img_url']."\" class=\"book-cover\"/>
                     <div class=\"title\">
                         <span class=\"name\">".$row['name']."</span>
                     </div>


                     <div class=\"no-of-books\">
                        <span class=\"number\">".$row['age']."</span>
                     </div>


                      <div class=\"no-of-books\">
                        <span>".$row['email']."</span>
                      </div>


                   <div class=\"no-of-books\">
                     <span>".$row['phone']."</span>
                   </div>

                </div>";

            }

         ?>

    </div></div>


<script>
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script>
function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
    <nav id="tf-footer" style="position: fixed;width: 100%; bottom: 0px;">
        <div class="container">
             <div class="pull-left">
                <p>2017 © SOS. Developed by Team vatOS for DJSCE Hackathon.</p>
            </div>
            <div class="pull-right">
                <ul class="social-media list-inline">
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                    <li><a href="#"><span class="fa fa-behance"></span></a></li>
                </ul>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/report_control.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>



  </body>
</html>
