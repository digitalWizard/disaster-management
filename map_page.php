
<?php
    require_once "php/DBConnect.php";
 ?>

<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Distress Map</title>
    <meta name="description" content="Your Description Here">
    <meta name="keywords" content="bootstrap themes, portfolio, responsive theme">
    <meta name="author" content="ThemeForces.Com">

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css"  href="css/map_style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <script type="text/javascript" src="js/modernizr.custom.js"></script>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div id="tf-home">
          <div class="overlay">
              <div id="sticky-anchor"></div>
              <nav id="tf-menu" class="navbar navbar-default stick">
                  <div class="container">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="homepage.html"><img src="img/main.png" style="position: fixed; top: 15px; left: 200px; height="50px"; width="50px;"></a>
                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                          <li><a href="homepage.html">Home</a></li>
                          <li><a href="map_page.php">Rescue Team</a></li>
                          <li><a href="search_disaster.php">Report</a></li>
                          <li><a href="Contactus.html">Contact</a></li>
                          <li><a href="news_feed.html">News Reports</a></li>
                        </ul>
                        <button class="btn btn-primary btn1">Login</button>
                      </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
              </nav>

          </div>
      </div>





    <div class="whole-container">


        <?php
            $query = "SELECT * FROM ping";

            $result = mysqli_query($conn, $query);

            while ($row = mysqli_fetch_assoc($result)) {

            }
         ?>


        <div class="notes_section">

            <div class="note_heading">
                <span>People who've tagged themselves :</span>
            </div>
<!--
            <div class="note">
                <span>Victim :</span>
                <span>Note</span>
            </div> -->

            <?php
                $query = "SELECT * FROM ping";

                $result = mysqli_query($conn, $query);

                while ($row = mysqli_fetch_assoc($result)) {
                    echo "<div class=\"note\">
                        <span>".$row['name']." :</span>
                        <span>".$row['note']."</span>
                        </div>";
                }
             ?>

        </div>


        <div class="location_updater">
            <span>Ping Your Location!</span>
            <img src="img/location.png" alt="">
        </div>


        <div class="map_section">
            <div id="map"></div>
            <!-- <script>
              function initMap() {
                var uluru = {lat: 19.1, lng: 72.8};

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 4,
                  center: uluru
                });



                //
                // google.maps.event.addListener(marker,'click',function() {
                //     map.setZoom(9);
                //     map.setCenter(marker.getPosition());
                //     infowindow.open(map,marker);
                // });
                //
                // var infowindow = new google.maps.InfoWindow({
                //     content: "Hello World!"
                // });

              }
            </script>
            <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaNufI1uaI_2zQ4IfVrGskcCWWRcriegg&callback=initMap">
            </script> -->



            <script type="text/javascript">
            var script = document.createElement('script');
            script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyBaNufI1uaI_2zQ4IfVrGskcCWWRcriegg&sensor=false&callback=initialize";
            document.body.appendChild(script);

            function initialize() {
                var map;
                var bounds = new google.maps.LatLngBounds();
                var mapOptions = {
                    mapTypeId: 'roadmap'
                };

                // Display a map on the page
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                map.setTilt(45);


                <?php

                    $query = "SELECT * FROM ping";

                    $result = mysqli_query($conn, $query);



                 ?>


                // Multiple Markers
                var markers = [
                    <?php

                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "['".$row['name']."', ".$row['lat'].", ".$row['lon']."],";
                        }

                     ?>
                    // ['London Eye, London', 51.503454,-0.119562],
                    // ['Palace of Westminster, London', 51.499633,-0.124755]
                ];

                // Info Window Content
                var infoWindowContent = [

                    <?php

                        $query = "SELECT * FROM ping";

                        $result = mysqli_query($conn, $query);

                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "['<div class=\"info_content\">' +
                            '<h3>".$row['name']."</h3>' +
                            '<p>".$row['note']."</p>' +
                            '</div>'],";
                        }

                     ?>
                    ['<div class="info_content">' +
                    '<h3>London Eye</h3>' +
                    '<p>The London Eye is a giant Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres (394 ft).</p>' +        '</div>'],
                    ['<div class="info_content">' +
                    '<h3>Palace of Westminster</h3>' +
                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                    '</div>']
                ];

                // Display multiple markers on a map
                var infoWindow = new google.maps.InfoWindow(), marker, i;

                // Loop through our array of markers & place each one on the map
                for( i = 0; i < markers.length; i++ ) {
                    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                    bounds.extend(position);
                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: markers[i][0]
                    });

                    // Allow each marker to have an info window
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infoWindow.setContent(infoWindowContent[i][0]);
                            infoWindow.open(map, marker);
                        }
                    })(marker, i));

                    // Automatically center the map fitting all markers on the screen
                    map.fitBounds(bounds);
                }

                // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
                var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                    this.setZoom(14);
                    google.maps.event.removeListener(boundsListener);
                });

            }
            </script>
        </div>


    </div>



    <!-- Modal Section -->

    <div class="location_modal">
        <form class="map_form">
          <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" class="form-control" id="Name" placeholder="Name">
          </div>
          <div class="form-group">
            <label for="Note">Note</label>
            <input type="text" class="form-control" id="Note" placeholder="Note">
          </div>
          <button type="button" class="btn btn-default modal_submit">Submit</button>
        </form>
    </div>

    <div class="modal_overlay"></div>


    <!-- Modal Ends -->

    <nav id="tf-footer" style="position: fixed;width: 100%; bottom: 0px;">
        <div class="container">
             <div class="pull-left">
                <p>2017 © SOS. Developed by Team vatOS for DJSCE Hackathon.</p>
            </div>
            <div class="pull-right">
                <ul class="social-media list-inline">
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                    <li><a href="#"><span class="fa fa-behance"></span></a></li>
                </ul>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Javascripts
    ================================================== -->
    <!-- <script type="text/javascript" src="js/main.js"></script> -->
    <script type="text/javascript" src="js/map_page_controller.js"></script>

  </body>
</html>
