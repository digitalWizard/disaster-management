$(function() {

    $(".location_updater").on("click", function() {

        $(".modal_overlay").css({
            "opacity": "0.5",
            "z-index": "0"
        });

        $(".location_modal").css({
            "top": "40%"
        });

    });

    $(".modal_overlay").on("click", function() {

        $(".modal_overlay").css({
            "opacity": "0",
            "z-index": "-2"
        });

        $(".location_modal").css({
            "top": "150%"
        });

    });

    $(".modal_submit").on("click", function() {

        var name = $("#Name").val();
        console.log(name);
        var note = $("#Note").val();
        var latitude = 0;
        var longitude = 0;

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;

            $.ajax({
                type: 'POST',
                url: "php/map_insert.php",
                data: {name: name, note: note, lat: latitude, lon: longitude},
                success: function(response) {
                    // console.log(response);
                }

            });
        }

        getLocation();

    });



    // $.ajax({
    //     type: 'POST',
    //     url: "php/retrieve_ping.php",
    //     // dataType: 'json',
    //     success: function(response) {
    //         console.log(JSON.parse(response));
    //     }
    // });


    // Asynchronously Load the map API


});
