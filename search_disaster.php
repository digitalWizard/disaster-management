
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Search Disaster</title>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">

        <script type="text/javascript" src="js/modernizr.custom.js"></script>

        <link rel="stylesheet" href="css/search_disaster.css">

    </head>
    <body>

        <div id="tf-home">
            <div class="overlay">
                <div id="sticky-anchor"></div>
                <nav id="tf-menu" class="navbar navbar-default stick">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand logo" href="homepage.html"><img src="img/main.png" style="position: fixed; top: 15px; left: 200px; height="50px"; width="50px;"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav navbar-right">
                            <li><a href="homepage.html">Home</a></li>
                            <li><a href="map_page.php">Rescue Team</a></li>
                            <li><a href="search_disaster.php">Report</a></li>
                            <li><a href="Contactus.html">Contact</a></li>
                            <li><a href="news_feed.html">News Reports</a></li>
                          </ul>
                          <button class="btn btn-primary btn1">Login</button>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>

            </div>
        </div>

        </div><br><br><br><br>
    </div>

    <div class="catalogue">

      <!-- <div class="book-card" id="45">
        <img src="../book-covers/1.jpg" class="book-cover"/>
        <div class="title">
            <span class="name">Harry Potter and the Philosopher's Stone.</span>
            <span class="author">- J. K. Rowling</span>
        </div>

        <div class="summary">
          <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim</span>
        </div>

        <div class="no-of-books">
          <span class="number">2</span>
          <span>Books Available</span>
        </div> -->



        <?php

            require_once "php/DBConnect.php";

            $query = "SHOW TABLES FROM codeshastra";

            $result = mysqli_query($conn, $query);

            while ($row = mysqli_fetch_array($result)){
                if ($row[0] != "users" && $row[0] != "ping") {
                    echo "<div class=\"book-card card\" id=".$row[0].">
                            ".$row[0]."
                            </div>";
                }
            }

        ?>

      </div>

      <!-- Catalogue goes here -->

    </div>



    <nav id="tf-footer" style="position: fixed;width: 100%; bottom: 0px;">
        <div class="container">
             <div class="pull-left">
                <p>2017 © SOS. Developed by Team vatOS for DJSCE Hackathon.</p>
            </div>
            <div class="pull-right">
                <ul class="social-media list-inline">
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                    <li><a href="#"><span class="fa fa-behance"></span></a></li>
                </ul>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Javascripts
    ================================================== -->
    <!-- <script type="text/javascript" src="js/main.js"></script> -->
    <script type="text/javascript" src="js/search_disaster.js"></script>

    </body>
</html>
