

<?php

    require_once "DBConnect.php";

    $query = "SELECT * FROM ping";

    $result = mysqli_query($conn, $query);

    $array = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $array[] = $row;
    }

    echo json_encode($array);

    mysqli_close($conn);

?>
